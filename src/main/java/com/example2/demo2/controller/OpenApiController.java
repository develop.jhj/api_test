package com.example2.demo2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class OpenApiController {

    private final RestTemplate restTemplate;

    @Autowired
    public OpenApiController(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @GetMapping("/call")
    public String getCall() {
        String weatherApiUrl = "https://apis.data.go.kr/1480523/MetalMeasuringResultService/MetalService?serviceKey=U8YRFA8AXziP1XgVWsCoaJg0kilvwzpNTDe08HhCl4fZrGvAHdBxh9k5NHQoT%2Fg8d9n4zfiC%2BRe3dmpMu36y2w%3D%3D&pageNo=1&numOfRows=10&resultType=JSON&date=20240524&stationcode=1&itemcode=90303&timecode=RH02";
        return restTemplate.getForObject(weatherApiUrl, String.class);
    }
}